package org.example.service;

import org.example.entities.Order;
import org.example.repository.ApplicationRepository;
import org.example.repository.OrderRepository;

import java.util.List;

public class OrderServiceImpl implements ApplicationRepository<Order> {
    OrderRepository orderRepository = new OrderRepository();

    @Override
    public void add(Order order) {
        orderRepository.add(order);

    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order findById(long id) {
        return orderRepository.findById(id);
    }

    @Override
    public void updateById(long id, Order order) {
        orderRepository.updateById(id, order);

    }

    @Override
    public void deleteById(long id) {
        orderRepository.deleteById(id);

    }

    @Override
    public List<Order> findByName(String name) {
        return orderRepository.findByName(name);
    }
}
