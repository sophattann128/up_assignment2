package org.example.service;

import org.example.entities.Product;
import org.example.repository.ApplicationRepository;
import org.example.repository.ProductRepository;

import java.io.IOException;
import java.util.List;

public class ProductServiceImpl implements ApplicationRepository<Product> {
    ProductRepository productRepository = new ProductRepository();

    @Override
    public void add(Product product) {

        productRepository.add(product);

    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(long id) {
        return productRepository.findById(id);
    }

    @Override
    public void updateById(long id, Product product) {
        productRepository.updateById(id, product);

    }

    @Override
    public void deleteById(long id) {
        productRepository.deleteById(id);

    }

    @Override
    public List<Product> findByName(String name) {
        return productRepository.findByName(name);
    }
}
