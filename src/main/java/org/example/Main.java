package org.example;

import org.example.config.DatabaseConfig;
import org.example.config.ObjectRecordWriter;
import org.example.entities.Order;
import org.example.entities.Product;
import org.example.repository.ApplicationRepository;
import org.example.repository.OrderRepository;
import org.example.repository.ProductRepository;
import org.example.service.OrderServiceImpl;
import org.example.service.ProductServiceImpl;

public class Main {
    public static void main(String[] args) {
//        ObjectRecordWriter objectRecordWriter = new ObjectRecordWriter();
        DatabaseConfig.createTable();
//        objectRecordWriter.createNewFile();

        Product product = new Product("orange", 3.5, "sophat", null);
        Product product1 = new Product("Banana", 4, "Bopha", "Dara");
        Order orderOrange = new Order(product, 90);

        ProductServiceImpl productService = new ProductServiceImpl();
        OrderServiceImpl orderService = new OrderServiceImpl();

//        productService.add(product);
//        productService.add(product1);
//        productService.updateById(2,product);
//        productService.deleteById(2);
//        System.out.println(productRepository.findById(2));
//        productService.findByName("a").forEach(System.out::println);
//        productService.findAll().forEach(System.out::println);
//        productService.updateById(5, orderOrange);


        orderService.add(new Order(product, 70));
//        orderService.add(new Order(product1, 30));
//        orderService.updateById(2, new Order(product, 90));
//        orderService.deleteById(1);
//        orderService.add(new Order(product, 50));

    }
}