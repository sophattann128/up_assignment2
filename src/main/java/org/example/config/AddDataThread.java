package org.example.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static org.example.config.DatabaseConfig.*;

public class AddDataThread implements Runnable {
    static Date date = new Date();
    static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    private int start;
    private int end;
    private CountDownLatch latch;
    static Connection conn = connection();


    public AddDataThread(int start, int end, CountDownLatch latch) {
        this.start = start;
        this.end = end;
        this.latch = latch;
    }

    public void run() {
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement("INSERT INTO product values (default, ?, ?, ?,?,?,?)");
            for (int i = start; i <= end; i++) {
                stmt.setInt(1, i);
                stmt.setString(1, "Orange");
                stmt.setDouble(2, 6.8);
                stmt.setString(3, "Sophat");
                stmt.setString(4, formatter.format(date));
                stmt.setString(5, "Kimly");
                stmt.setString(6, formatter.format(date));
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            latch.countDown();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int numThreads = 3;
        int numRecords = 1000000;
        CountDownLatch latch = new CountDownLatch(numThreads);

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < numThreads; i++) {
            int start = 1;
            int end = (i + 1) * (numRecords / numThreads);

            Thread t = new Thread(new AddDataThread(start, end, latch));
            t.start();
        }
        latch.await(); // wait for all threads to finish

        long duration = System.currentTimeMillis() - startTime;
        System.out.println("Added " + numRecords + " records using " + numThreads + " threads in " + duration + " ms");
    }
}


