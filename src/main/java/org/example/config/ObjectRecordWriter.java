package org.example.config;

import org.example.entities.Record;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ObjectRecordWriter {

    private static final int MAX_RECORDS_PER_FILE = 5000;
    private static int recordCount = 0;
    private static int fileCount = 1;
    private static String filename = "records";
    static ObjectOutputStream outputStream = null;

    static {
        createNewFile();
    }

    private static ArrayList<Record> records = new ArrayList<>();


    public static void addRecord(Record record) {
        try {
            records.add(record);
            writeToFile();
            recordCount++;
            if (recordCount >= MAX_RECORDS_PER_FILE) {
                writeToFile();
                createNewFile();
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void createNewFile() {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
            String newFilename = filename + "-" + fileCount + ".txt";
            outputStream = new ObjectOutputStream(new FileOutputStream(newFilename));
            recordCount = 0;
            fileCount++;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeToFile() throws IOException {
        for (Record record : records) {
            outputStream.writeObject(record);
        }
        outputStream.flush();
        records.clear();

    }

    public static List<Record> readRecords(String fileName) {
        Path filePath = Path.of(filename);
        if (Files.exists(filePath)) {
            try {
                try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
                    while (true) {
                        Record record = (Record) in.readObject();
                        records.add(record);
                    }
                } catch (EOFException ignored) {

                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return records;
    }

    public static List<Record> readObjectsFromFile(String fileName) {
        List<Record> records = new ArrayList<>();
        try {
            Path filePath = Path.of(fileName);
            if (Files.exists(filePath)) {
                try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
                    while (true) {
                        try {
                            Object obj = in.readObject();
                            records.add((Record) obj);
                        } catch (ClassNotFoundException e) {
                            // Ignore class not found exception
                        } catch (IOException e) {
                            // End of file reached
                            break;
                        }
                    }
                }
            } else {
                throw new IOException("File does not exist: " + fileName);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return records;
    }

}





