package org.example.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConfig {

    static final String driverName = "jdbc:postgresql://";
    static final String hostName = "localhost:";
    static final String port = "33001/";
    static final String dbName = "my_db";
    static final String username = "my_user";
    static final String password = "my_password";

    public static Connection connection() {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection(driverName + hostName + port + dbName,
                            username, password);

        } catch (SQLException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return c;

    }
    public static void createTable(){
        try {
            Connection con = DatabaseConfig.connection();
            Statement stmt = con.createStatement();
            String productSql = "CREATE TABLE  IF NOT EXISTS product " +
                    "(id serial PRIMARY KEY ," +
                    " product_name           varchar(50)    NOT NULL, " +
                    " price  real," +
                    " created_by   varchar(10)    NOT NULL," +
                    " created_date   varchar(10)    NOT NULL," +
                    " updated_by   varchar(10)  ," +
                    " updated_date   varchar(10)  " +

                    ")";
            String orderSql = "CREATE TABLE  IF NOT EXISTS orders " +
                    "(id serial PRIMARY KEY ," +
//                    " product_id     Int not null , " +
                    "product_id INTEGER REFERENCES product(id)," +
                    " qty  int," +
                    " created_by   varchar(10)    NOT NULL," +
                    " created_date   varchar(10)    NOT NULL," +
                    " updated_by   varchar(10)  ," +
                    " updated_date   varchar(10)  " +
                    ")";
            stmt.executeUpdate(productSql);
            stmt.executeUpdate(orderSql);


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
