package org.example.repository;

import org.example.config.ObjectRecordWriter;
import org.example.entities.Order;
import org.example.entities.Product;
import org.example.entities.Record;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import static org.example.config.DatabaseConfig.connection;
import static org.example.repository.Access_type_order.INSERT_ORDER;

enum Access_type_order {
    INSERT_ORDER,
    UPDATE_ORDER,
    DELETE_ORDER,
    SELECT_ORDER
}


public class OrderRepository implements ApplicationRepository<Order> {
    static Connection con = connection();
    static Date date = new Date();
    static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    ProductRepository productRepository = new ProductRepository();
    Record record = null;
    ObjectRecordWriter objectWriter = new ObjectRecordWriter();


    @Override
    public List<Order> findAll() {
        List<Order> orders = new ArrayList<>();
        try {
            PreparedStatement productStatement =
                    con.prepareStatement("select * from orders");
            ResultSet orderResultSet = productStatement.executeQuery();
            record = new Record(1, new Date().toString(), "Seyha", INSERT_ORDER.name());
            objectWriter.addRecord(record);
            while (orderResultSet.next()) {
                long orderId = orderResultSet.getLong("id");
                int qty = orderResultSet.getInt("qty");
                String createdBy = orderResultSet.getString("created_by");
                String createdAt = orderResultSet.getString("created_date");
                String updatedBy = orderResultSet.getString("updated_by");
                String updatedDate = orderResultSet.getString("updated_date");
                for (Product product : productRepository.findAll()) {
                    Order order = new Order(orderId, product, qty, createdBy, createdAt, updatedBy, updatedDate);
                    orders.add(order);
                    break;
                }

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        return orders;
    }

    private long findObjectId(Product product) {
        long productId = 0;
        try {
            PreparedStatement preparedStatement =
                    con.prepareStatement("select id from product where product_name=?");
            preparedStatement.setString(1, product.getName());
            ResultSet productResultSet = preparedStatement.executeQuery();
            while (productResultSet.next()) {
                productId = productResultSet.getLong("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return productId;

    }

    @Override
    public void add(Order order) {
        try {
            long productId = findObjectId(order.getProduct());
            System.out.println(productId);
            // Insert the orders
            PreparedStatement orderStatement = con.prepareStatement("INSERT INTO orders VALUES (default, ?, ?,?,?,?,?)");
            orderStatement.setLong(1, productId);
            orderStatement.setInt(2, order.getQty());
            orderStatement.setString(3, order.getProduct().getCreatedBy());
            orderStatement.setString(4, formatter.format(date));
            orderStatement.setString(5, order.getProduct().getUpdatedBy());
            orderStatement.setString(6, order.getProduct().getUpdatedAt());
            orderStatement.executeUpdate();

            record = new Record(1, new Date().toString(), "Kimly", INSERT_ORDER.name(), productId);
            ObjectRecordWriter.addRecord(record);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public void updateById(long id, Order order) {

        try {
            con.setAutoCommit(false);
            // Update the orders
            PreparedStatement orderStatement = con.prepareStatement("UPDATE orders SET qty =?, updated_by=?, updated_date=? WHERE id=?");
            orderStatement.setInt(1, order.getQty());
            orderStatement.setString(2, order.getUpdatedBy());
            orderStatement.setString(3, formatter.format(date));
            orderStatement.setLong(4, id);
            orderStatement.executeUpdate();
            con.commit();
            record = new Record(1, new Date().toString(), "Kimly", INSERT_ORDER.name(), id);
            ObjectRecordWriter.addRecord(record);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public List<Order> findByName(String keyword) {
        return null;
    }

    private long findObjectId(long id) {
        long productId = 0;
        try {
            PreparedStatement preparedStatement =
                    con.prepareStatement("select id from orders where id=?");
            preparedStatement.setLong(1, id);
            ResultSet orderResultSet = preparedStatement.executeQuery();
            while (orderResultSet.next()) {
                productId = orderResultSet.getLong("product_id");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return productId;

    }

    @Override
    public Order findById(long id) {
        long product_id = findObjectId(id);
        Order order = null;
        try {
            Product product = productRepository.findById(product_id);
            PreparedStatement preparedStatement =
                    con.prepareStatement("select * from orders where id=?");
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            ResultSet orderResultSet = preparedStatement.executeQuery();
            while (orderResultSet.next()) {
                long orderId = orderResultSet.getLong("id");
                int qty = orderResultSet.getInt("qty");
                String createdBy = orderResultSet.getString("created_by");
                String createdAt = orderResultSet.getString("created_date");
                String updatedBy = orderResultSet.getString("updated_by");
                String updatedDate = orderResultSet.getString("updated_date");
                order = new Order(orderId, product, qty, createdBy, createdAt, updatedBy, updatedDate);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return order;
    }
    @Override
    public void deleteById(long id) {
        try {
            con.setAutoCommit(false);
            PreparedStatement orderStatement =
                    con.prepareStatement("delete from orders where id = ?;");
            orderStatement.setLong(1, id);
            orderStatement.execute();
            con.commit();
            record = new Record(1, new Date().toString(), "Kimly", INSERT_ORDER.name(), id);
            ObjectRecordWriter.addRecord(record);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
