package org.example.repository;

import org.example.entities.Product;

import java.util.List;

public interface ApplicationRepository<T> {
    void add(T object);
    List<T> findAll();
    T findById(long id);
    void updateById(long id, T object);
    void deleteById(long id);
    List<T>findByName(String name);


}
