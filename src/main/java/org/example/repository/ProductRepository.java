package org.example.repository;

import org.example.config.DatabaseConfig;
import org.example.config.ObjectRecordWriter;
import org.example.entities.Order;
import org.example.entities.Product;
import org.example.entities.Record;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static org.example.config.DatabaseConfig.*;
import static org.example.repository.Access_type.*;

enum Access_type{
    INSERT_PRODUCT,
    UPDATE_PRODUCT,
    DELETE_PRODUCT,
    SELECT_PRODUCT
}

public class ProductRepository implements ApplicationRepository<Product> {
    static Connection con = connection();
    static Date date = new Date();
    static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");


    private String getInput(String msg) {
        Scanner scanner = new Scanner(System.in);
        System.out.print(msg);
        return scanner.nextLine();
    }


    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        try {
            PreparedStatement productStatement =
                    con.prepareStatement("select * from product");
            ResultSet productResultSet = productStatement.executeQuery();
            Record record = new Record(1, new Date().toString(),"Seyha", INSERT_PRODUCT.name());
            ObjectRecordWriter.addRecord(record);
            while (productResultSet.next()) {
                long productId = productResultSet.getLong("id");
                String name = productResultSet.getString("product_name");
                double price = productResultSet.getDouble("price");
                String createdBy = productResultSet.getString("created_by");
                String updatedBy = productResultSet.getString("updated_by");
                String updatedDate = productResultSet.getString("updated_date");
                Product product = new Product(productId, name, price, createdBy, updatedBy, updatedDate);
                products.add(product);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return products;
    }

    private long findObjectId(Product product) {
        long productId = 0;
        try {
            PreparedStatement preparedStatement =
                    con.prepareStatement("select id from product where product_name=?");
            preparedStatement.setString(1, product.getName());
            ResultSet productResultSet = preparedStatement.executeQuery();
            while (productResultSet.next()) {
                productId = productResultSet.getLong("id");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return productId;

    }

    @Override
    public void add(Product product) {
        try {

            PreparedStatement productStatement =
                    con.prepareStatement("INSERT INTO product values (default, ?, ?, ?,?,?,?)");
            productStatement.setString(1, product.getName());
            productStatement.setDouble(2, product.getPrice());
            productStatement.setString(3, product.getCreatedBy());
            productStatement.setString(4, formatter.format(date));
            productStatement.setString(5, product.getUpdatedBy());
            productStatement.setString(6, product.getUpdatedAt());
            productStatement.executeUpdate();
            long productId = findObjectId(product);
            Record record = new Record(1, new Date().toString(),"Seyha", INSERT_PRODUCT.name(),productId);
            ObjectRecordWriter.addRecord(record);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    @Override
    public void updateById(long id, Product product) {

        try {
            con.setAutoCommit(false);
            PreparedStatement productStatement = con.prepareStatement("UPDATE product SET product_name=?, price=?, updated_by =?, updated_date=? WHERE id=?");
            productStatement.setString(1, product.getName());
            productStatement.setDouble(2, product.getPrice());
            productStatement.setString(3, product.getUpdatedBy());
            productStatement.setString(4, formatter.format(date));
            productStatement.setLong(5, id);

            productStatement.executeUpdate();

            long productId = findObjectId(product);
            Record record = new Record(1, new Date().toString(),"Kimly", UPDATE_PRODUCT.name(),productId);
            ObjectRecordWriter.addRecord(record);


            // Commit the changes
            con.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("You need to delete the order that reference to this product first.");
        }


    }

    @Override
    public List<Product> findByName(String keyword) {
        List<Product> products = new ArrayList<>();
        try {
            PreparedStatement preparedStatement =
                    con.prepareStatement("Select * from product where product.product_name like CONCAT( '%',?,'%')");
            preparedStatement.setString(1, keyword);
            preparedStatement.execute();

            ResultSet productResultSet = preparedStatement.executeQuery();
            while (productResultSet.next()) {
                long productId = productResultSet.getLong("id");
                String name = productResultSet.getString("product_name");
                double price = productResultSet.getDouble("price");
                String createdBy = productResultSet.getString("created_by");
                String updatedBy = productResultSet.getString("updated_by");
                String updatedDate = productResultSet.getString("updated_date");
                Product product = new Product(productId, name, price, createdBy, updatedBy, updatedDate);
                products.add(product);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return products;
    }

    @Override
    public Product findById(long id) {
        Connection con = DatabaseConfig.connection();
        Product product = null;
        try {
            PreparedStatement preparedStatement =
                    con.prepareStatement("select * from product where id=?");
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            ResultSet productResultSet = preparedStatement.executeQuery();
            while (productResultSet.next()) {
                long productId = productResultSet.getLong("id");
                String name = productResultSet.getString("product_name");
                double price = productResultSet.getDouble("price");
                String createdBy = productResultSet.getString("created_by");
                String updatedBy = productResultSet.getString("updated_by");
                String updatedDate = productResultSet.getString("updated_date");
                product = new Product(productId, name, price, createdBy, updatedBy, updatedDate);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return product;
    }

    @Override
    public void deleteById(long id) {
        try {
            con.setAutoCommit(false);
            PreparedStatement productStatement =
                    con.prepareStatement("delete from product where id = ?;");
            productStatement.setLong(1, id);
            productStatement.execute();
            con.commit();
//            long productId = findObjectId(product);
            Record record = new Record(1, new Date().toString(),"Sophat", DELETE_PRODUCT.name(),id);
            ObjectRecordWriter.addRecord(record);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

}
