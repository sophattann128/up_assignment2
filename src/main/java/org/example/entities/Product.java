package org.example.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ToString
public class Product {
    private long id;
    private String name;
    private double price;
    private List<Order> orders = new ArrayList<>();
    private String createdBy;
    private String createdAt;
    private String updatedBy;
    private String updatedAt;

    public Product(String name, double price, String createdBy, String updatedBy, String updatedAt) {
        this.name = name;
        this.price = price;
        this.createdBy = createdBy;
        this.createdAt = new Date().toString();
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
    }

    public Product(long id, String name, double price, String createdBy, String updatedBy, String updatedAt) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.orders = new ArrayList<>();
        this.createdBy = createdBy;
        this.createdAt = new Date().toString();
        this.updatedBy = updatedBy;
        this.updatedAt = updatedAt;
    }
    public Product(String name, double price, String createdBy, String updatedBy) {
        this.name = name;
        this.price = price;
        this.orders = new ArrayList<>();
        this.createdBy = createdBy;
        this.createdAt = new Date().toString();
        this.updatedBy = updatedBy;
    }

    public Product(long id, String name, double price, String createdBy, String updatedBy, List<Order> orders) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.orders = orders;
        this.createdBy = createdBy;
        this.createdAt = new Date().toString();
        this.updatedBy = updatedBy;
    }


    public void addOrder(Order order){
        orders.add(order);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
