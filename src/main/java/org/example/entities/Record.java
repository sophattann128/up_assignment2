package org.example.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Record implements Serializable {
    private long id;
    private String createdAt;
    private String createdBy;
    private String access_type;
    private long entity_id;

    public Record(long id, String createdAt, String createdBy, String access_type) {
        this.id = id;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.access_type = access_type;
    }
}
